---
weight: 200
title: Pairing
---

# Pairing Lighthouse Controllers or Trackers

To pair lighthouse-capable hardware such as the Valve Index or HTC
Vive Pro controllers, or VIVE Trackers you'll need enough receivers (one for
each device).

Receivers can be either:
- watchman dongles (plugged in your computer, preferably not on an USB hub)
- if you have a lighthouse HMD (e.g. Valve Index, HTC Vive, Bigscreen Beyond),
  there will be two already built-in (for both controllers)

## Pairing via SteamVR

You can follow the normal pairing process by launching SteamVR, and opening the
pairing window (in the Devices menu).

If however for some reason this does not work (in particular, if the pairing window
[does not render](https://github.com/ValveSoftware/SteamVR-for-Linux/issues/531)),
you can work around it by doing the pairing process via `lighthouse_console`.

## Pairing via `lighthouse_console`

In a terminal, launch `lighthouse_console` by running
```
~/.steam/steam/steamapps/common/SteamVR/tools/lighthouse/bin/linux64/lighthouse_console
```

### Identifying available receivers

You'll be greeted by a shell which starts by listing the serial number of attached receivers (both
dongles and built-in, if your HMD is connected).

Selecting a receiver is done by running `serial <serialnumber>` in the opened shell.
When pairing new devices,
you'll want to find a receiver that isn't paired with a device already. After a
receiver is selected, you can run `identifycontroller` to check if the receiver
is paired with a device (assuming the device is on).

In general, built-in HMD receivers will have longer serial numbers than dongle
ones.

If either you want to *replace* previously paired devices (e.g. when changing
controllers) or you're struggling to identify which receiver to use, run
`unpairall`. This will unpair all lighthouse devices!

### Pairing a device

Once you have selected an available receiver with `serial <serialnumber>`:

1. Turn on your device
2. Put it in pairing mode (e.g. hold System+B for Valve controllers), the LED
   should be blinking blue
3. Run the `pair` command

After a few seconds, the LED should turn green, and the device is now paired.

Do this for each device you want to pair (remember to use a different receiver
for each device), and exit the shell by running `quit`.
