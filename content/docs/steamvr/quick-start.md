---
weight: 200
title: Quick Start
---

# Getting Started with SteamVR on Linux

## Check your Video Drivers

- {{< icon name="nvidia" >}} Nvidia users: use the version that's [officially listed as production](https://www.nvidia.com/en-us/drivers/unix/). new feature/beta versions tend to have issues.
- {{< icon name="amd" >}} AMD users: use Mesa 24+ radeonsi, kernel 6.7.3+ if possible, stay away from amdpro.

## Dual-GPU Systems

- AMD+AMD laptop or desktop: Amazing choice, you have nothing to do.
- Desktop with iGPU and dGPU of different brands: Disable your iGPU in BIOS.
- Laptop with Nvidia dGPU:
  - **Option 1 (Ubuntu-derivatives)**: Try the official Ubuntu solution `prime-select nvidia` before launching Steam.
  - **Option 2**: Use `optimus-manager-qt` ([AUR](https://aur.archlinux.org/packages/optimus-manager-qt)) and switch to Nvidia before launching Steam.
  - **Option 3**: Launch steam, steamvr and all games with `prime-run` (Launch options: `prime-run %command%`)
  - **Extra tips**:
    - Make ABSOLUTELY sure you don't have weird stuff like amdvlk, including lib32 installed when you have Nvidia (check `ls /usr/share/vulkan/icd.d`)
    - Stick to the PRODUCTION branch of Nvidia drivers, newer ones may or may not work.

## Installing Steam

If the `steam` system package is available on your distro, we recommend starting with that.

**Flatpak Steam is not recommended**, as there are be capabilities that cannot be granted inside Flatpak, resulting in ghosting/stutter.

Launch Steam and install SteamVR. If it repeatedly nags you about sudo upon launch, setcap the vrcompositor manually:

```bash
sudo setcap CAP_SYS_NICE=eip ~/.local/share/Steam/steamapps/common/SteamVR/bin/linux64/vrcompositor-launcher
```

(Default install location on most distros)

## Setting up your HMD

**Valve Index, Vive, Vive Pro**: These will work just by plugging in.

**Vive Pro 2** will need the driver from here: [VivePro2-Linux-Driver on GitHub](https://github.com/CertainLach/VivePro2-Linux-Driver)

**Bigscreen Beyond** requires a patched kernel: [Bigscreen Beyond Kernel Patch](https://gist.githubusercontent.com/TayouVR/af8635a4b8e1d02d038be1be1d221c83/raw/3806a6ff0a03721904164277d7523d43f7ca383c/bigscreenBeyond.patch)

For any of the above headsets with {{< icon name="nvidia" >}} Nvidia, you will get a smoother experience with Monado + OpenComposite (use Envision for easy setup), albeit this combo does not work with all VR titles.

For many games, the "Legacy Reprojection" option in the per-app video settings may provide a smoother experience.

**Quest 1/2/3/Pro, Pico 3/4, Vive Focus 3/XR Elite**:

Your best bet is [the latest ALVR release](https://github.com/alvr-org/ALVR/releases)

Also check the [ALVR wiki](https://github.com/alvr-org/ALVR/wiki), especially the [Linux Troubleshooting](https://github.com/alvr-org/ALVR/wiki/Linux-Troubleshooting) section.

## Optional: Auto-Restart Script

The `startvr` script [here](https://gist.github.com/galister/a85135f4a3aca5208ba4091069ab2222) is meant to make launching SteamVR less tedious.

It can be used with both ALVR and wired headsets.

What it does:

- Whitelist drivers for SpaceCal and ALVR so these will never get blocked
- Apply bindings spam patch by Plyshka
- Setcap the compositor after a SteamVR update
- Start SteamVR and restart it for you in case of a crash
- Prevent SteamVR processes from getting stuck and putting SteamVR in an inconsistent state

Extra steps when using with ALVR:

- Under `Installation` tab, register the ALVR driver
- Under `Settings`, `Steamvr Launcher` section, set `Driver launch action` to `No action`.

Note: Cloud sync can get stuck and prevent SteamVR from launching. I recommend disabling cloud sync for SteamVR.

If Steam is installed in a non-standard location, change the variable on the top of the script.

To use the script, simply launch it from a terminal. It will ask for sudo password in case your SteamVR has been freshly installed or updated and it needs to `setcap` the compositor.

ALVR-only: In case a SteamVR error appears; Ctrl-C the script, open ALVR dashboard and launch SteamVR from there once. You then don't have to open ALVR until there's a SteamVR update.

When restarting SteamVR, simply exit it using your overlay and it'll be restarted for you. To really exit SteamVR, first Ctrl-C the script.

## Optional: Disable SteamVR dashboard

The SteamVR dashboard has some major issues with high CPU usage, occasional freezing and sometimes going completely unresponsive.

Many choose to disable SteamVR dashboard and instead use something like WlxOverlay-S for desktop & game library access.

To do this, simply remove the execute permission from `vrwebhelper`:

```bash
chmod -x ~/.steam/steam/steamapps/common/SteamVR/bin/vrwebhelper/linux64/vrwebhelper

```

(To re-enable, run the same command but with `+x` instead of `-x`.)
