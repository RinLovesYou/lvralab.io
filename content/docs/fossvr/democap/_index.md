---
title: DEMoCap - Drag[en]gine Motion Capture
weight: 100
---

# DEMoCap - Drag[en]gine Motion Capture

- [Website](https://dragondreams.ch/index.php/democap)
- [Wiki/Documentation/Tutorials](https://developer.dragondreams.ch/wiki/doku.php/democap:main)
- [Github Repository](https://github.com/LordOfDragons/democap)

DEMoCap is an open source application allowing to do motion capture using VR equipment. The main use case is for game development but you can capture animations also for movie projects or creating small videos. DEMoCap supports motion capture of humandoid and especially non-humanoid characters. Typically captured animations are imported into Blender3D but there is also support for live-streaming captured animations.

![Capture Session Playback.](https://dragondreams.ch/wp-content/uploads/2022/01/multichar-1.jpeg)

## Hardware

DEMoCap can be used to do motion capture for full body, upper body, first person views and anything you can imagine. DEMoCap uses whatever VR hardware is available at the time of calibration. The following non-exhaustive list of VR equipment can be used by DEMoCap:
- HMD: For capturing head movement, eye movement and facial movement (for example HTC VIVE Pro Eye)
- Controllers: For capturing hand movement and finger movement (for example Index Controllers)
- VIVE Trackers: For capturing waist, chest, elbow, feet and knee movement.

DEMoCap supports automatic setups up to 11-point tracking. Manually you can create all kinds of tracking setups with as many trackers your VR runtime supports and custom motion transfer (for example tails, wings, multiple arm pairs).

## Setup

See the [Download Section](https://dragondreams.ch/index.php/democap/#download) on the website and select one of the download sources for your OS. The store links take care of installing and updating DEMoCap and the game engine dependency for you.

If you want to setup DEMoCap yourself install first the latest [Drag[en] gine Game Engine Installer](https://dragondreams.ch/index.php/dragengine/#downloads-dragengine). Then download the _DEMoCap-1.1.delga_ file from the Multiplatform section. You can now run DEMoCap by double clicking on the _DEMoCap-1.1.delga_ file. Optionally you can also run DEMoCap from console using _delauncher DEMoCap-1.1.delga_

Now you have to prepare Blender3D for use with DEMoCap. See [Blender3D Script Installation](https://developer.dragondreams.ch/wiki/doku.php/democap:blender_democaptools#installation) for how to install the necessary addons (_Drag[en]gine Blender Export Scripts_ and _Blender DEMoCap Tools Addon_).

To start a motion capture project you have to first prepare your character model in Blender3D. See [Prepare Character for Motion Capture](https://developer.dragondreams.ch/wiki/doku.php/democap:preparecharacter) on the Wiki Documentation section.

Once done you can create a motion capture project in DEMoCap and get going. See [Project Management](https://developer.dragondreams.ch/wiki/doku.php/democap:projectmanagement#project_management) to create a new project and the various pages under [Documentation and Tutorials](https://developer.dragondreams.ch/wiki/doku.php/democap:main#documentation) for information about the individual steps.

If you have questions or problems drop by the [discord channel](https://discord.gg/Jeg62ns) for help.