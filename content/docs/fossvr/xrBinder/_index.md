---
weight: 900
title: xrBinder
---

# xrBinder
- [xrBinder GitLab repository](https://gitlab.com/mittorn/xrBinder)
- [xrBinder example_configs](https://gitlab.com/mittorn/xrBinder/-/tree/master/example_configs)

> xrBinder is an OpenXR binding layer that is designed to manage key bindings for XR applications. It acts as a bridge between input actions and various VR/AR devices, providing a way to map specific hardware inputs to in-game actions. This is particularily useful for when the default mappings of an application may not align with your preferences or specific hardware capabilities.

{{% hint danger %}}
**Warning**

xrBinder is still considered alpha-quality and highly experimental.
Certain bindings may not work when they are hard coded to the application, such as the [Index Controller bindings in VRChat](https://docs.vrchat.com/docs/valve-index#binding-customization-notes) for `Jump`, `Mic Toggle`, `Gesture Toggle` and `Action Menu Left / Right`.
{{% /hint %}}

## Building from source

The project uses submodules to include dependencies such as imgui. They will need to be initialized and updated after cloning the repository.

```bash
git clone https://gitlab.com/mittorn/xrBinder.git
cd xrBinder
git submodule update --init --recursive
```

It is recommended to create a build subdirectory:

```bash
mkdir build
cd build
cmake ..
make
```

Once building is complete, the shared object (.so file) will be located in the `<build>/XR_APILAYER_NOVENDOR_xr_binder` directory, alongside a few binaries and a `manifest.json`.

## Importing as an implicit layer

The [API Layer Interaction documentation for OpenXR](https://github.com/KhronosGroup/OpenXR-SDK-Source/blob/main/specification/loader/api_layer.adoc) states that layers can be either implicit, where they are automatically enabled, and explicit, where they must be manually enabled. We will set this up as an implicit layer for convenience.

This gives us a choice of directories to copy the contents of the `<build>/XR_APILAYER_NOVENDOR_xr_binder` directory to, assuming the environment variable `XR_API_LAYER_PATH` is not set:

```
/usr/local/etc/openxr/1/api_layers/implicit.d
/usr/local/share/openxr/1/api_layers/implicit.d
/etc/openxr/1/api_layers/implicit.d
/usr/share/openxr/1/api_layers/implicit.d
$HOME/.local/share/openxr/1/api_layers/implicit.d
```

If the directory structure does not exist, it must be manually recreated.

Once the files are copied over to the implicit.d directory, make sure to double-check the manifest.json so that it matches the **API Layer Manifest File Format** in the OpenXR API Layer documentation.

### Example manifest.json

```json
{
  "file_format_version": "1.0.0",
  "api_layer": {
    "name": "XR_APILAYER_NOVENDOR_xr_binder",
    "disable_environment": "XR_APILAYER_NOVENDOR_xr_binder_DISABLE",
    "api_version": "1.0",
    "implementation_version": "1",
    "description": "Advanced layer for binding actions'",
    "library_path": "./libxrBinder_module.so"
  }
}

```

## Configuration

xrBinder reads and stores its configuration in `$XDG_CONFIG_HOME/xrBinder` or `$HOME/.config/xrBinder`. If either directories do not exist, they must be manually created.

Copy the [example_configs directory](https://gitlab.com/mittorn/xrBinder/-/tree/master/example_configs) from the xrBinder gitlab repository and place it and its contents in the xrBinder config directory.

Depending on which controllers you use, pick the corresponding .ini template from the `source_templates` subdirectory and append its entire content into the `xrBinder.ini` file.

## Usage

To verify if the module was loaded, check your opencomposite logs or your system logs after you've launched your application. If xrBinder was loaded successfully, these two lines should be present as an example:

```
[2024-10-10 01:49:03.230] CreateOpenXRBackend:154	- Num layers available: 1
[2024-10-10 01:49:03.230] CreateOpenXRBackend:162	- Layer: XR_APILAYER_NOVENDOR_xr_binder
```

If this is the case, we can begin using xrBinder. You may have noticed these two lines in `xrBinder.ini` which come by default:

```bash
# UDP port for ipc
serverPort = 9011
# run ipc_server <port> and gui/cli client if set to bus mode or just client in server
ipcMode = bus
```

In this example, we are expected to run the `ipc_server` binary in the implicit.d directory we copied our files from the build into on port 9011 from the terminal.

```bash
./ipc_server 9011
```

With the server and the application where we want to edit our keybinds running in the Background, we can now use the xrBinder GUI by executing the `client_gui` binary.

![xrBinder GUI](/images/xrbinder_gui.png "Example usage of xrBinder to bind gesture_toggle to the left A button")

- Type in the port to connect to (9011 in our case) and hit Connect
- Under the "Windows" expander, click "Session". This will open a new window in the xrBinder GUI.
- Load the data in each tab of the session window
- Choose the action and source to dictate what action to bind the button to, then hit Apply.
- The binding should be applied immediately. You can hit "Update data" below to refresh the list of bindings that have been applied.
