---
weight: 50
title: Motoc
---

# Motoc

- [Motoc GitHub repository](https://github.com/galister/motoc)

Motoc is a tool for calibrating devices of different tracking technologies to work together.

With support for both one-shot and continuous modes, Motoc is a complete solution for all your calibration needs.

Example use cases:
- Use a WiVRn HMD with lighthouse trackers
- Use a WMR HMD with lighthouse trackers

Please take a look at the GitHub Readme for a comprehensive guide.

## Support

Reach out in the `monado` or `wivrn` room in Discord or Matrix.