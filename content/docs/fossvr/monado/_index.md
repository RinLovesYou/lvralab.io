---
weight: 50
title: Monado
---

# Monado

- [Monado home page](https://monado.freedesktop.org/)
- [Monado GitLab repository](https://gitlab.freedesktop.org/monado/monado)

![The Legendary Big M](https://gitlab.freedesktop.org/uploads/-/system/project/avatar/2685/monado_icon_medium.png?width=128)

> Monado is an open source XR runtime delivering immersive experiences such as VR and AR on mobile, PC/desktop, and other devices. Monado aims to be a complete and conformant implementation of the OpenXR API made by Khronos. The project is currently being developed for GNU/Linux and aims to support other operating systems such as Windows in the near future.

Essentially, Monado is an open source OpenXR implementation, it can be used as an alternative to SteamVR.

Depending on the game, Monado can offer a better overall experience (if with less features) compared to SteamVR, or it might not work at all.

Monado should always be utilized with the environment variable `XRT_COMPOSITOR_COMPUTE=1` to avoid stuttering when application is below maximum refresh rate of the HMD.

Check your logs and ensure that the keywords REALTIME and COMPUTE vulkan queue has been created as opposed to MEDIUM and GRAPHICS queue types before opening issues about failing reprojection.
If your monado-service is unable to acquire these properties in the debug logs `sudo setcap 'cap_sys_nice=eip' <monado-service-binary>` to ensure it can request the priority it needs.

Monado's space may be modified with the environment variables OXR_TRACKING_ORIGIN_OFFSET_X=0.0, OXR_TRACKING_ORIGIN_OFFSET_Y=1.0, OXR_TRACKING_ORIGIN_OFFSET_Z=0.0 to force a defined space or origin offset throughout the session. Useful for seated mode configurations, offset units are floating point in meters, positive or negative.

Monado is made for PCVR headsets, if you have a standalone headset you can check out [WiVRn](/docs/fossvr/wivrn/) or [ALVR](/docs/steamvr/alvr/).

## Steam

To use Monado as the OpenXR runtime with Steam, or if you're planning to use the SteamVR lighthouse driver in Monado, make sure to [run the room setup first](/docs/steamvr/).

In order to use the SteamVR lighthouse driver in Monado, you just need to set the environment variable `STEAMVR_LH_ENABLE=true`.

OpenComposite is REQUIRED to operate Monado in conjunction with Steam games. Proton requires a functional OpenVR API to utilize OpenXR at all and Proton itself is required for VR to function as normal wine does not carry any of the needed patches nor does "protonified" wine something like Lutris would provide.

## Envision

[Envision](/docs/fossvr/envision/) provides a fairly low-barrier setup and development of both Monado and OpenComposite on most any modern distro.