---
weight: 50
title: WiVRn
---

# WiVRn

- [WiVRn GitHub repository](https://github.com/WiVRn/WiVRn)

![The WiVRn mascot](https://github.com/WiVRn/WiVRn/blob/master/images/wivrn.svg?raw=true)

> WiVRn wirelessly connects a standalone VR headset to a Linux computer. You can then play PCVR games on the headset while processing is done on the computer.

WiVRn is based on [Monado](/docs/fossvr/monado/) and can be used with [OpenComposite](/docs/fossvr/opencomposite/) to support a majority of titles available for SteamVR. A wide range of standalone headsets are supported.

## Installing WiVRn

We recommend using [Envision](/docs/fossvr/envision/) to install and launch WiVRn & OpenComposite. Envision will handle all configuration seamlessly and avoids many of the pitfalls of a manual setup.

Alternatively, you can use the [WiVRn Flatpak](https://flathub.org/apps/io.github.wivrn.wivrn) which includes OpenComposite as part of it. Processes in Flatpak cannot take advantage of setcap, and so your performance may suffer when compared to Envision.

## General WiVRn Notes

WiVRn uses Avahi for network discovery. Ensure it is running with the following terminal command:
```bash
systemctl enable --now avahi-daemon
```
(If Avahi is not available, the IP address of the server must be entered into the client.)

If a firewall is installed, make sure ports 5353/UDP and 9757/UDP+TCP are open for Avahi and WiVRn itself, respectively.

If using Nvidia proprietary drivers, have [Monado Vulkan Layers](https://gitlab.freedesktop.org/monado/utilities/vulkan-layers) installed, otherwise games will crash with a segmentation fault.

For audio in WiVRn, you will need to assign applications, or the system as a whole, to output audio to the virtual output "WiVRn" which is created upon connection between the server and the headset.

It is possible to use WiVRn Flatpak with Steam Flatpak, though Steam Flatpak isn't generally recommended for VR. If you are using Steam Flatpak, you just need to grant it access to the relevant paths:

```bash
flatpak override --user --filesystem=xdg-config/openxr:ro com.valvesoftware.Steam
flatpak override --user --filesystem=xdg-config/openvr:ro com.valvesoftware.Steam
flatpak override --user --filesystem=xdg-run/wivrn com.valvesoftware.Steam
flatpak override --user --filesystem=/var/lib/flatpak/app/io.github.wivrn.wivrn:ro com.valvesoftware.Steam
```

As an added bonus to the above, this command will save you the effort of having to set launch options for every single VR game in your Steam library:

```bash
flatpak override --user --env=PRESSURE_VESSEL_FILESYSTEMS_RW=/run/user/1000/wivrn/comp_ipc:/var/lib/flatpak/app/io.github.wivrn.wivrn com.valvesoftware.Steam
```

## Wired WiVRn

You can use WiVRn with a cable instead of Wi-Fi. Use a 5Gbps cable & port at the very least.

WiVRn version: 0.19+

Using Envision:
- Connect the headset via USB.
- In Envision, click the `Start WiVRn Client (Wired)` button.
  - If it's not there, you may need to update Envision.

Manual steps:
- Connect the headset via USB.
- If WiVRn is running on the headset, close it now.
- While in the system lobby of the headset, run the following `adb` commands on the PC:
  ```bash
  adb reverse tcp:9757 tcp:9757
  adb shell am start -a android.intent.action.VIEW -d "wivrn+tcp://127.0.0.1" org.meumeu.wivrn
  ```

## WiVRn + Lighthouse driver

This section covers using WiVRn with any Lighthouse-tracked device: Index/Vive controllers, Vive/Tundra trackers, etc.

WiVRn version: 0.19+

You must have SteamVR installed (no need to run it).

In Envision, set WiVRn Profile as such:
- XR Service CMake Flags:
  - `WIVRN_FEATURE_STEAMVR_LIGHTHOUSE=ON`
- Environment Variables:
  - `WIVRN_USE_STEAMVR_LH=1`
  - `LH_DISCOVER_WAIT_MS=6000`

Perform a **Clean Build** after changing the CMake flags!

If not using Envision, simply pass `-DWIVRN_FEATURE_STEAMVR_LIGHTHOUSE=ON` to CMake and export the above environment variables before starting `wivrn-server`.

You can use [Motoc](/docs/fossvr/motoc/) to calibrate the two tracking technologies to work together.

Discovery happens only on first connection, so be sure to **have all Lighthouse devices powered on and in line-of-sight of your base stations before connecting the headset!**

Once a device is discovered, you may power it off and on at-will.

Once video appears in the headset, check `motoc monitor` to make sure your devices all show up.

If one or more devices are missing, try:
- Spread the devices out more; Lighthouse devices get discovered quicker if they're not piled up on each other. Simply strapping them on is good, too.
- Increase `LH_DISCOVER_WAIT_MS`, though this delays the client on first connection.

To re-discover devices, restart WiVRn server.

At this point, your Lighthouse devices will be randomly floating about somewhere. To calibrate them, follow the guide in the [Motoc README](https://github.com/galister/motoc/blob/main/README.md).
