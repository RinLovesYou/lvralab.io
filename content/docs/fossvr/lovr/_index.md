---
weight: 100
title: LÖVR
---

# LÖVR

- [LÖVR home page](https://lovr.org/)
- [LÖVR GitLab repository](https://github.com/bjornbytes/lovr)

> You can use LÖVR to easily create VR experiences without much setup or programming experience. The framework is tiny, fast, open source, and supports lots of different platforms and devices.

The LÖVR engine is a Lua XR engine, written in C, which supports overlays on monado. Very simple and fun to implement whatever you would like.

- [lovr-playspace](/docs/fossvr/lovr/lovr-playspace/) A barebones room boundary built on the LÖVR engine.